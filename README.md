# Cars App

## Main features
- Displaying a list of cars with infinite scrolling.
- Implement unit test for usecases layer.

## Tech Stack
- kotlin 
- Clean Architecure with MVVM.
- koin for di.
- Rxjava2 and Rx-kotlin.
- Retrofit2.
- Material.
- recycler view with ListAdapter.
- Timber for logging.
- ConstraintLayout.
- viewmodel and live data.
- Glide.
- Jetpack Navigation.

## unit test
- Junit4.
- Mockito.


 
