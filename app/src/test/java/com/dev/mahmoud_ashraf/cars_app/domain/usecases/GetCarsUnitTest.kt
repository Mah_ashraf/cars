package com.dev.mahmoud_ashraf.cars_app.domain.usecases

import com.dev.mahmoud_ashraf.cars_app.data.entities.BaseResponse
import com.dev.mahmoud_ashraf.cars_app.data.entities.CarEntity
import com.dev.mahmoud_ashraf.cars_app.domain.core.RxImmediateSchedulerRule
import com.dev.mahmoud_ashraf.cars_app.domain.repositories.CarsRepository
import io.reactivex.Single
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers.anyInt
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class GetCarsUnitTest {

    @Mock
    lateinit var carsRepository: CarsRepository

    // to test rx java with different schedulers
    @Rule
    @JvmField
    var rxImmediateSchedulerRule = RxImmediateSchedulerRule()

    @Test
    fun `fetchCars() with pageNumber will return cars List in this page`() {

        // arrange
        val page = 1
        val list = mutableListOf<CarEntity>()

        list.add(CarEntity("image1.jpg", "",11,"",true))



        Mockito.`when`(carsRepository.getCarsList(anyInt()))
            .thenReturn(
                Single.just(BaseResponse<List<CarEntity>>(status = 200,data = listOf( CarEntity(
                    "image1.jpg", "",11,"",true
                ))))
            )

        // act
        val resultObserver = getCarsUseCase(page, carsRepository).test()

        // assert
        val expected = mutableListOf<CarEntity>()
        expected.add(CarEntity(
            "image1.jpg", "",11,"",true
        ))

        resultObserver.assertValue(expected)
        resultObserver.dispose()
    }


}