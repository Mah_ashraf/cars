package com.dev.mahmoud_ashraf.cars_app.data.entities

import com.google.gson.annotations.SerializedName

data class CarEntity(
    @SerializedName("brand")
    val brand: String,
    @SerializedName("constractionYear")
    val constractionYear: String?,
    @SerializedName("id")
    val id: Int,
    @SerializedName("imageUrl")
    val imageUrl: String?,
    @SerializedName("isUsed")
    val isUsed: Boolean
)
data class BaseResponse<T>(
    @SerializedName("data")
    val `data`: T,
    @SerializedName("status")
    val status: Int
)

