package com.dev.mahmoud_ashraf.cars_app.data.repositories

import com.dev.mahmoud_ashraf.cars_app.data.entities.BaseResponse
import com.dev.mahmoud_ashraf.cars_app.data.entities.CarEntity
import com.dev.mahmoud_ashraf.cars_app.data.gateways.ServerGateway
import com.dev.mahmoud_ashraf.cars_app.domain.repositories.CarsRepository
import io.reactivex.Single

class CarsRepositoryImp(private val serverGateway: ServerGateway) : CarsRepository {

    override fun getCarsList(page: Int): Single<BaseResponse<List<CarEntity>>> {
        return serverGateway.getCarsList(page = page)
    }

}