package com.dev.mahmoud_ashraf.cars_app.presentation.features.home

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.dev.mahmoud_ashraf.cars_app.R
import com.dev.mahmoud_ashraf.cars_app.databinding.HomeFragmentBinding
import com.dev.mahmoud_ashraf.cars_app.presentation.core.EndlessRecyclerViewScrollListener
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber

class HomeFragment : Fragment() {

    private val homeViewModel by viewModel<HomeViewModel>()
    private val adapter by lazy { CarsAdapter() }

    private lateinit var binding: HomeFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.home_fragment, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        setUpVenuesRecycler()

        homeViewModel.errors.observe(viewLifecycleOwner, Observer {
            Toast.makeText(requireContext(), it, Toast.LENGTH_SHORT).show()
        })

        homeViewModel.carsLiveData.observe(viewLifecycleOwner, Observer {
            adapter.submitList(it)
        })
    }

    private fun setUpVenuesRecycler() {
        binding.swipeRf.setOnRefreshListener {
            binding.swipeRf.isRefreshing = false
            adapter.submitList(listOf())
            homeViewModel.PAGE_NUM = 1
            homeViewModel.fetchCars()

        }
        binding.carsRecycler.adapter = adapter
        binding.carsRecycler.itemAnimator = null
        adapter.onItemClicked = { _, _ -> }

      binding.carsRecycler.addOnScrollListener(object : EndlessRecyclerViewScrollListener(
        binding.carsRecycler.layoutManager as LinearLayoutManager
      ){
          override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView?) {
              Timber.i("load more... ")
              homeViewModel.fetchCars()
          }

      })
    }





}