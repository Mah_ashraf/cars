package com.dev.mahmoud_ashraf.cars_app.presentation.features.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.dev.mahmoud_ashraf.cars_app.data.entities.CarEntity
import com.dev.mahmoud_ashraf.cars_app.domain.repositories.CarsRepository
import com.dev.mahmoud_ashraf.cars_app.domain.usecases.getCarsUseCase
import com.dev.mahmoud_ashraf.cars_app.presentation.core.SingleLiveEvent
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers

class HomeViewModel(private val carsRepository: CarsRepository) : ViewModel() {

    private val compositeDisposable by lazy { CompositeDisposable() }

    private val _carsLiveData = MutableLiveData<List<CarEntity>>()
    val carsLiveData : LiveData<List<CarEntity>>
            get() = _carsLiveData

     val errors = SingleLiveEvent<String>()


     var PAGE_NUM = 1

    init {
        fetchCars()
    }

    fun fetchCars() {
        getCarsUseCase(page = PAGE_NUM, repository = carsRepository)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { cars ->
                    val temp = carsLiveData.value?.toMutableList() ?: mutableListOf()
                    temp.addAll(cars)
                    _carsLiveData.value = temp
                    PAGE_NUM++
                },
                { throwable ->
                    throwable.printStackTrace()
                    errors.value = "Something wrong happened."
                }

            ).addTo(compositeDisposable)
    }

    override fun onCleared() {
        compositeDisposable.dispose()
        super.onCleared()
    }

}