package com.dev.mahmoud_ashraf.cars_app.domain.repositories

import com.dev.mahmoud_ashraf.cars_app.data.entities.BaseResponse
import com.dev.mahmoud_ashraf.cars_app.data.entities.CarEntity
import io.reactivex.Single

interface CarsRepository {

    fun getCarsList(page: Int): Single<BaseResponse<List<CarEntity>>>
}