package com.dev.mahmoud_ashraf.cars_app.presentation.di

import com.dev.mahmoud_ashraf.cars_app.data.gateways.ServerGateway
import com.dev.mahmoud_ashraf.cars_app.data.repositories.CarsRepositoryImp
import com.dev.mahmoud_ashraf.cars_app.domain.repositories.CarsRepository
import org.koin.dsl.module

val repositoryModule = module {
    single {
        provideCarsRepository(
            get()
        )
    }
}

fun provideCarsRepository(serverGateway: ServerGateway): CarsRepository {
    return CarsRepositoryImp(serverGateway)
}
