package com.dev.mahmoud_ashraf.cars_app.presentation.features.home

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.dev.mahmoud_ashraf.cars_app.R

class HomeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.home_activity)
    }

}