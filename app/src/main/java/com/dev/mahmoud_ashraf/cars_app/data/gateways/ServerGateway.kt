package com.dev.mahmoud_ashraf.cars_app.data.gateways

import com.dev.mahmoud_ashraf.cars_app.data.entities.BaseResponse
import com.dev.mahmoud_ashraf.cars_app.data.entities.CarEntity
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface ServerGateway {

    @GET("api/v1/cars")
    fun getCarsList(@Query("page") page: Int): Single<BaseResponse<List<CarEntity>>>


}
