package com.dev.mahmoud_ashraf.cars_app.presentation.features.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.dev.mahmoud_ashraf.cars_app.data.entities.CarEntity
import com.dev.mahmoud_ashraf.cars_app.databinding.ActorItemViewBinding

class CarsAdapter :
    ListAdapter<CarEntity, CarsAdapter.ActorViewHolder>(ActorsDiffCallback()) {

    var onItemClicked: ((position: Int, actor: CarEntity) -> Unit)? = null


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ActorViewHolder {
        return ActorViewHolder(
            ActorItemViewBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ActorViewHolder, position: Int) {
        val actorListItem = getItem(position)
        holder.bind(actorListItem, onItemClicked, position)
    }

    class ActorViewHolder(
        private val binding: ActorItemViewBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        fun bind(
            carEntity: CarEntity,
            onItemClicked: ((position: Int, carEntity: CarEntity) -> Unit)?,
            position: Int
        ) {
            binding.nameText.text = carEntity.brand.plus(" - ").plus(carEntity.constractionYear)
                .plus(if(carEntity.isUsed)" - Used" else " - not Used")

            Glide.with(binding.profileImage)
                .load(carEntity.imageUrl)
                .placeholder(android.R.color.darker_gray)
                .error(android.R.color.darker_gray)
                .override(300)
                .thumbnail(0.1f)
                .into(binding.profileImage)

            binding.itemCardView.setOnClickListener {
                onItemClicked?.invoke(position, carEntity)
            }

            binding.executePendingBindings()


        }
    }

    class ActorsDiffCallback : DiffUtil.ItemCallback<CarEntity>() {
        override fun areItemsTheSame(
            oldItem: CarEntity,
            newItem: CarEntity
        ): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(
            oldItem: CarEntity,
            newItem: CarEntity
        ): Boolean {
            return oldItem == newItem
        }


    }
}