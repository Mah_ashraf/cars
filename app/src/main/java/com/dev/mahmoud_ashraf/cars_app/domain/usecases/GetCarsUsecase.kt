package com.dev.mahmoud_ashraf.cars_app.domain.usecases

import com.dev.mahmoud_ashraf.cars_app.data.entities.CarEntity
import com.dev.mahmoud_ashraf.cars_app.domain.repositories.CarsRepository
import io.reactivex.Single

fun getCarsUseCase(
    page: Int?,
    repository: CarsRepository
): Single<List<CarEntity>> {
    return repository
        .takeIf { page != null }
        ?.getCarsList(page!!)
        ?.map {
            it.data
        }
        ?: Single.error(Exception())

}
